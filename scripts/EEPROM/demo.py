from machine import I2C, Pin
import EEPROM_CAT24C02 #For 32bit or 4KB EEPROM library. Change this according to your library name.
import ubinascii
import time

sda=machine.Pin(0)
scl=machine.Pin(1)
i2c=machine.I2C(0,sda=sda, scl=scl, freq=100000)

print("I2C Devices : ")
print(i2c.scan())

# Output from xxd -p fmceeprom.bin without line breaks
buf = ubinascii.unhexlify("01000012000100ec02020df9f600b400ab00bd0000000000e80302020d34bb014a0139015a0100000000e80302020de20d02b0047404ec0400000000000001020dfdf30300000000000000000000000001020de01004b400b000b70000000000010001020dfbf505000000000000000000000000fa0208c23a5c44140000800a008200007e0000000000000000000000010a00000000e14b61726c737275686520496e73746974757465206f6620546563686e6f6c6f6779d254616e64656d2d4c204144432d426f617264c131ce54414e44454d4c2d414443303031c0c100dc0000000000000000000000000000000000000000000000000000000000000000")

i2caddr=0x50 #Set the I2C address of your EEPROM.

eeprom = EEPROM_CAT24C02.CAT24C02(i2c,i2caddr)

# Write the EEPROM with the data from buf
#eeprom.write(0, buf)

# Read and print 32Bytes starting from memory address 0
print(eeprom.read(0, len(buf)))
